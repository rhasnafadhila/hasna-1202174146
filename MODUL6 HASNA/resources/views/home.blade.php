@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row ">
        @foreach($posts as $post)
        <div class="col-md-6">
            <div class="card my-2">
                <div class="card-header">
                    <img src="{{$post->user->avatar}}" width="10%" style="border-radius: 50%;">
                    <b> {{$post->user->name}}</b>
                </div>

                <div class="card-body">
                    <center>
                    <img src="{{$post->image}}" width="100%">
                    </center>
                </div>
                <div class="card-footer">
                    <a href="{{ url('comment/'.$post->id) }}">
                        <i class="fa fa-heart-o" style="font-size:24px; color: black;"></i>
                    </a> 
                    <a href="#detail_komentar">
                        <i class="fa far fa-comment-o" style="font-size:24px; color: black;"></i>
                    </a>
                    <b>{{$post->likes}} Like</b><br>
                    <p>
                        <b>{{$post->user->email}}</b>
                        {{$post->caption}}
                    </p>
                    <p>
                        @foreach($komentars as $komentar)
                            @if($komentar->post_id == $post->id)
                                <b>{{$komentar->email}}</b> {{$komentar->comment}}<br>
                            @endif
                        @endforeach
                    </p>
                    <form action="{{route('comment.add')}}" method="POST">  
                        @csrf
                        <input type="hidden" name="post_id" value="{{$post->id}}">
                        <input type="text" class="form-control" name="komentar" id="detail_komentar" required placeholder="Add a comment..">
                        <button type="submit" class="btn btn-success" name="create_komentar" value="kirim">Send</button>
                    </form>
                </div>

            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
