<?php 
	session_start();
	if (!(isset($_SESSION["id_user"]))) {
		header("Location: home.php");
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>Profile</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #800080;">
	  <a class="navbar-brand" href="home.php"><img src="EAD.png" width="100"></a>

	  <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 80%;">
	    <ul class="navbar-nav mr-auto float-right">
	      <li class="nav-item active">
	        <a class="nav-link text-white" href="cart.php"><img src="shopping-cart.png" width="20"></a>
	      </li>
	      <li class="nav-item dropdown">
	        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username']; ?></a>
	        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
	          <a class="dropdown-item" href="profile.php">Profile</a>
	          <div class="dropdown-divider"></div>
	          <a class="dropdown-item" href="logout.php">Log Out</a>
	        </div>
	      </li>
	    </ul>
	  </div>
	</nav>
	<div class="card my-3 mx-auto" style="width: 50%;">
	  <div class="card-body">
	  	<?php 
	  		$conn = mysqli_connect('localhost','root','','db_wad_04');
	  		$idUser = $_SESSION['id_user'];

			$login = "SELECT * FROM `users` WHERE `id`='".$idUser."'";

			$query = mysqli_query($conn, $login);

			$hasil = mysqli_fetch_assoc($query);
	  	 ?>
	    <form action="profilefunction.php" method="post">
	    	<div class="row text-center">
				<div class="col-12">
					<h4>Profile</h4>
				</div>
			</div>
	    	<div class="row mb-2">
				<div class="col-sm-3">
					<label>Email</label>
				</div>
				<div class="col-sm-9">
					<label><?php echo $hasil['email']; ?></label>
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-sm-3">
					<label>Username</label>
				</div>
				<div class="col-sm-9">
					<input type="text" name="userProfile" required placeholder="Dummy" class="form-control" value="<?php echo $hasil['username'];?>">
				</div>
			</div>
			<div class="row">
				<div class="col-sm-3">
					<label>Mobile Number</label>
				</div>
				<div class="col-sm-9">
					<input type="number" name="numProfile" required placeholder="Mobile Number" class="form-control">
				</div>
			</div>
			<hr>
			<div class="row mb-3">
				<div class="col-sm-3">
					<label>New Password</label>
				</div>
				<div class="col-sm-9">
					<input type="password" name="newPass" placeholder="New Password" class="form-control">
				</div>
			</div>
			<div class="row mb-3">
				<div class="col-sm-3">
					<label>Confirm Password</label>
				</div>
				<div class="col-sm-9">
					<input type="password" name="confirmPass" placeholder="Confirm Password" class="form-control">
				</div>
			</div>
			<div class="row text-center mb-1">
				<div class="col-12">
					<input type="submit" name="update" value="Send" class="btn btn-block" class="form-control" style="background-color:#800080;color: white;">
				</div>
			</div>
			<div class="row text-center mb-1">
				<div class="col-12">
					<a href="home.php" class="btn btn-block" style="background-color:#800080; color: white;">Cancel</a>
				</div>
			</div>
	    </form>
	  </div>
	</div>
		<div class="footer text-center py-3">© EAD STORE
		</div>
</body>
	<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>