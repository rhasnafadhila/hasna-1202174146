<?php 
   $total = 0;
   if (!empty($_POST['checkbox1'])) {
   		$total += $_POST['checkbox1'];
   }
   if (!empty($_POST['checkbox2'])) {
   		$total += $_POST['checkbox2'];
   }
   if (!empty($_POST['checkbox3'])) {
   		$total += $_POST['checkbox3'];
   }
   if (!empty($_POST['checkbox4'])) {
   		$total += $_POST['checkbox4'];
   }
   if (!empty($_POST['checkbox5'])) {
   		$total += $_POST['checkbox5'];
   }
   if ($_POST['member'] == "Ya") {
   		$total *= 0.9;
   }
 ?>

<!DOCTYPE html>
<html>
<head>
  <title>Struk Belanja</title>

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>
<body>

	<div class="row" style="color:grey; font-family: sans-serif;font-size: 20px;margin: 60px">
		<div class="col-sm-4"></div>
		<div class="col-sm-4 text-center">
			<h1>Transaksi Pemesanan</h1>
			<p>Terimakasih telah berbelanja dengan Kopi Susu Duarrr!</p>
			<h1><?php echo $total; ?></h1>
			<div class="row">
				<div class="col-sm-4 form-label"><b>ID</b></div>
				<div class="col-sm-8"><?php echo $_POST['nomerorder']; ?></div>
			</div>
			<div class="row">
				<div class="col-sm-4 form-label"><b>Nama</b></div>
				<div class="col-sm-8"><?php echo $_POST['namapemesan']; ?></div>
			</div>
			<div class="row">
				<div class="col-sm-4 form-label"><b>Email</b></div>
				<div class="col-sm-8"><?php echo $_POST['email']; ?></div>
			</div>
			<div class="row">
				<div class="col-sm-4 form-label"><b>Alamat</b></div>
				<div class="col-sm-8"><?php echo $_POST['Alamat']; ?></div>
			</div>
			<div class="row">
				<div class="col-sm-4 form-label"><b>Member</b></div>
				<div class="col-sm-8"><?php echo $_POST['member']; ?></div>
			</div>
			<div class="row">
				<div class="col-sm-4 form-label"><b>Pembayaran</b></div>
				<div class="col-sm-8"><?php echo $_POST['Metode']; ?></div>
			</div>
		</div>
		<div class="col-sm-4"></div>
	</div>

  
  


 
</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>

</html>