<?php 
  session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
  <link rel="icon" href="dicoding.png">
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
</head>
<body>
  <nav class="navbar navbar-expand-lg navbar-light" style="background-color:#800080;">
    <a class="navbar-brand" href="home.php"><img src="EAD.png" width="100"></a>

    <div class="collapse navbar-collapse" id="navbarSupportedContent" style="margin-left: 80%;">
      <ul class="navbar-nav mr-auto float-right">
        <?php if (isset($_SESSION["id_user"])) {?>
          <li class="nav-item active">
            <a class="nav-link text-white" href="cart.php"><img src="shopping-cart.png" width="30"></a>
          </li>
          <li class="nav-item dropdown active">
            <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?php echo $_SESSION['username']; ?></a>
              <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="profile.php">Profile</a>
                  <div class="dropdown-divider"></div>
                  <a class="dropdown-item" href="logout.php">Log Out</a>
              </div>
          </li>
        <?php } else {?>
          <li class="nav-item active">
              <a class="nav-link text-white" href="#" data-toggle="modal" data-target="#modalLogin">Login</a>
            </li>
            <li class="nav-item active">
              <a class="nav-link text-white" href="#" data-toggle="modal" data-target="#modalRegis">Register</a>
            </li>
        <?php } ?>
      </ul>
    </div>
  </nav>
 <div class="card my-3 mx-auto text-white" style="width: 50rem;background-color: #800080;">
    <div class="card-body">
      <span class="align-middle">
        <h1>Hello Coders</h1>
        <p>Welcome to our store,  please look for the product you might buy</p>
      </span>
    </div>
  </div>

</div>
  <div class="row mx-auto" style="width: 51rem;">
<div class="col-sm-auto">
      <div class="card mb-3" style="width: 15rem;border-color:#800080; ">
        <img src="https://tekshapers.com/uploads/blog_image/15362384091533896513blog-sco2.jpg" class="card-img-top" alt="...">
        <div class="card-body" style="height: 15rem;">
          <h5 class="card-title">Learning Basic Web Programming</h5>
          <p class="card-text"><b>Rp.100.000,-</b></p>
          <p class="card-text">Web programming refers to the writing, markup and coding involved in Web development, which includes Web content, Web client and server scripting and network security.</p>
        </div>
        <div class="card-body">
          <a href="functioncart.php?cartProduct=Learning+Basic+Web+Programming&cartPrice=100000&buy=beli" class="card-link btn" role="button" style="width: 12rem; background-color:#800080; color: white;">Buy</a>
        </div>
      </div>
    </div>
    <div class="col-sm-auto">
      <div class="card mb-3" style="width: 15rem;border-color:#800080;">
        <img src="https://www.oracle.com/a/ocom/img/hp11-intl-java-logo.jpg" class="card-img-top" alt="...">
        <div class="card-body" style="height: 15rem;">
          <h5 class="card-title">Starting Programming in Java</h5>
          <p class="card-text"><b>Rp.150.000,-</b></p>
          <p class="card-text" style="height: 3rem;">Learn to code in Java and improve your programming and problem-solving skills. You will learn to design algorithms as well as develop and debug programs.</p>
        </div>
        <div class="card-body">
          <a href="functioncart.php?cartProduct=Starting+Programming+In+Java&cartPrice=150000&buy=beli" class="card-link btn" role="button" style="width: 12rem; background-color:#800080; color: white;">Buy</a>
        </div>
      </div>
    </div>
    <div class="col-sm-auto">
      <div class="card mb-3" style="width: 15rem; border-color: #800080;">
        <img src="https://teknologi.id/wp-content/uploads/2018/11/Python-700x394.jpg" class="card-img-top" alt="...">
        <div class="card-body" style="height: 15rem;">
          <h5 class="card-title">Learning Programming in phyton</h5>
          <p class="card-text"><b>Rp.200.000,-</b></p>
          <p class="card-text" style="height: 3rem;">Python is a general-purpose, versatile and popular programming language.
       </p>
        </div>
        <div class="card-body">
          <a href="functioncart.php?cartProduct=Learning+Programming+In+Phyton&cartPrice=200000&buy=beli" class="card-link btn" role="button" style="width: 12rem; background-color:#800080; color: white;">Buy</a>
        </div>
      </div>
    </div>
  </div>


      </div>
    </div>
  </div>

  <div class="modal fade" id="modalRegis" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="post" action="functionhome.php">
          <div class="modal-header">
            <h5 class="modal-title" id="registerModalLabel">Register</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="recipient-email" class="col-form-label">Email Address</label>
              <input type="email" class="form-control" id="recipient-register-email" placeholder="Enter Email"  name="regisEmail" required>
            </div>
            <div class="form-group">
              <label for="recipient-username" class="col-form-label">Username</label>
              <input type="text" class="form-control" id="recipient-register-username" placeholder="Enter Username"  name="regisUser" required>
            </div>
            <div class="form-group">
              <label for="recipient-password" class="col-form-label">Password</label>
              <input type="password" class="form-control" id="recipient-register-password" placeholder="Password" name="regisPass" required>
            </div>
            <div class="form-group">
              <label for="message-repassword" class="col-form-label">Confirm Password</label>
              <input type="password" class="form-control" id="recipient-register-repassword" placeholder="Confirm Password" name="regisConPass" required>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <input type="submit" class="btn" value="Register" name="register" style="color: white; background-color:#800080; " >
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modalLogin" tabindex="-1">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        <form method="post" action="functionhome.php">
        <div class="modal-header">
          <h5 class="modal-title" id="loginModalLabel">Login</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
              <label for="recipient-email" class="col-form-label">Email Address</label>
              <input type="email" class="form-control" id="recipient-login-email" placeholder="Enter Email" name="loginEmail" required>
            </div>
            <div class="form-group">
              <label for="recipient-password" class="col-form-label">Password</label>
              <input type="password" class="form-control" id="recipient-login-password" placeholder="Password" name="loginPass" required>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <input type="submit" class="btn" value="Login" name="login" style="color: white;background-color:#800080;">
        </div>
        </form>
      </div>
    </div>
  </div>
   <div class="footer text-center py-3">© EAD STORE
      <a href="#"> </a>
    </div>
   
</body>
  <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</html>