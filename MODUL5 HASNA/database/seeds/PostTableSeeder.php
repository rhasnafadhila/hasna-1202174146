<?php

use Illuminate\Database\Seeder; 
use Illuminate\Support\Facades\DB; 
use Illuminate\Support\Str;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([ 
            'user_id' =>1,
            'caption' => 'Tim Young-Lex', 
            'image' => 'hasna1.jpeg', 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' =>1,
            'caption' => 'D M T 2018 ', 
            'image' => 'hasna2.jpeg', 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Hari hari Simulasi', 
            'image' => 'hasna3.jpeg', 
        ]); 

        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Panassss', 
            'image' => 'hasna4.jpeg', 
        ]);

        DB::table('posts')->insert([ 
            'user_id' => 1,
            'caption' => 'Ciwi ciwi', 
            'image' => 'hasna5.jpg', 
        ]); 
    }
}
