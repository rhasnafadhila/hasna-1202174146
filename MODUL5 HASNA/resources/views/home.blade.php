@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        @foreach($posts as $post)
        <div class="col-md-8">
            <div class="card my-2">
                <div class="card-header"><img src="hasna6.jpeg" width="10%" style="border-radius: 100%;">{{Auth::user()->name}}</div>

                <div class="card-body">
                    <center>
                    <img src="{{$post->image}}" width="600">
                    </center>
                </div>
                <div class="card-footer">
                    {{Auth::user()->email}}<br>
                    {{$post->caption}}
                </div>

            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection
