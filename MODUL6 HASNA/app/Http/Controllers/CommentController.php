<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\KomentarPost;
use Auth;

class CommentController extends Controller
{
    public function addComment(Request $request)
    {
        $coments = new KomentarPost();
        $coments->user_id = Auth::User()->id;
        $coments->post_id = $request->post_id;
        $coments->comment = $request->komentar;

        $coments->save();
        return redirect()->route('post.show',$request->post_id);
    }

    public function addLike($post_id)
    {
    	$post = Post::where('id', $post_id)->get();
    	// dd($post);
        $posts = Post::findOrFail($post_id);

        $posts->likes = $post[0]->likes + 1;

        $posts->save();
        return redirect()->route('post.show',$post_id);
    }
}
